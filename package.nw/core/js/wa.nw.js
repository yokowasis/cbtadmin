const { Client, Buttons } = require('whatsapp-web.js');

const client = new Client();

let statusWA = "Belum Aktif";
let qrWA = "";

client.on('qr', (qr) => {
    // Generate and scan this code with your phone
	qrWA = qr;
	console.log(qr);
});


client.on('ready', () => {
	statusWA = "Aktif";
    console.log('Client is ready!');
});

function findSiswa(s) {
	daftarSiswa = localStorage.getItem("daftarsiswa");
	daftarSiswa = JSON.parse(daftarSiswa);
	var result = [];
	for (var i = 0; i < daftarSiswa.length; i++) {
		if (daftarSiswa[i][1].includes(s) || daftarSiswa[i][2].includes(s)) {
			result.push(daftarSiswa[i]);
		}
	}
	return result;
}

const waDialog = (command,msg) => {
	var s = msg.body.match(new RegExp(`(${command} )(.*)`, 'i'))
	var namasiswa = s[2];

	var searchResult = findSiswa(namasiswa);

	if (searchResult.length > 0) {
		let body = [];
		let s = "";
		searchResult.forEach(siswa => {
			body.push({
				body : `!${command.replace(/ /g, "")} ${siswa[1]}`,
			});
			s += `${siswa[1]}/${siswa[2]}\n`;
		});
		client.sendMessage(msg.id.remote,new Buttons(s,body,`Pilih Siswa Untuk di *${command}* pada Mapel : ${aktif}\n================\n`,"CBTAdmin Bimasoft v13.0.0"));
	} else {
		client.sendMessage(msg.id.remote,new Buttons("Siswa Tidak Ditemukan",[],"Pilih Siswa Untuk di *${command}*","CBTAdmin Bimasoft v13.0.0"));
	}		
}

client.on('message', msg => {
    if (msg.body == '!ping') {
        msg.reply('pong');
    } else
    if (msg.body.includes('token') === true) {
		fetch(`${localStorage.getItem('urlserver')}/wp-content/themes/unbk/api-18575621/generatetoken.php`,{
			method: "GET",
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
		})
		.then(res => res.text())
		.then(
			(result) => {
				msg.reply(result);
			}
		)
    } else
    if (msg.body.includes('!resetlogin') === true) {
		let s = msg.body.match(/(!resetlogin) (.*)/)
		let user = s[2];
		let mapel = encodeURI(aktif);
		let server = encodeURI(localStorage.getItem('idserver')); 
		let url = `${localStorage.getItem('urlserver')}wp-content/themes/unbk/api-18575621/reset.php?mapel=${mapel}&user=${user}&server=${server}`;
		fetch(url)
		.then(res => res.text())
		.then(
			(result) => {
				client.sendMessage(msg.id.remote,"Siswa Berhasil di Reset Login","CBTAdmin Bimasoft v13.0.0");
			}
		)
		.catch((err)=>{
			console.log (err);
			client.sendMessage(msg.id.remote,"Siswa Gagal di Reset Login","CBTAdmin Bimasoft v13.0.0");
		})
    } else
    if (msg.body.includes('!resetselesai') === true) {
		let s = msg.body.match(/(!resetselesai) (.*)/)
		let user = s[2];
		let mapel = encodeURI(aktif);
		let server = encodeURI(localStorage.getItem('idserver')); 
		let url = `${localStorage.getItem('urlserver')}wp-json/bimasoft-unbk/v1/server-resetstatus/${server}/${mapel}/${user}`;
		fetch(url)
		.then(res => res.text())
		.then(
			(result) => {
				client.sendMessage(msg.id.remote,"Siswa Berhasil di Reset Selesai","CBTAdmin Bimasoft v13.0.0");
			}
		)
		.catch((err)=>{
			client.sendMessage(msg.id.remote,"Siswa Gagal di Reset Selesai","CBTAdmin Bimasoft v13.0.0");
		})
    } else
    if (msg.body.includes('!forceselesai') === true) {
		let s = msg.body.match(/(!forceselesai) (.*)/)
		let user = s[2];
		let mapel = encodeURI(aktif);
		let server = encodeURI(localStorage.getItem('idserver')); 
		let url = `${localStorage.getItem('urlserver')}wp-json/bimasoft-unbk/v1/server-forceselesai/${server}/${mapel}/${user}/null`;
		fetch(url)
		.then(res => res.text())
		.then(
			(result) => {
				client.sendMessage(msg.id.remote,"Siswa Berhasil di Force Selesai","CBTAdmin Bimasoft v13.0.0");
			}
		)
		.catch((err)=>{
			client.sendMessage(msg.id.remote,"Siswa Gagal di Force Selesai","CBTAdmin Bimasoft v13.0.0");
		})
    } else
    if (msg.body.includes('reset login') === true) {
		waDialog("reset login",msg);
    } else
    if (msg.body.includes('reset selesai') === true) {
		waDialog("reset selesai",msg);
    } else
    if (msg.body.includes('force selesai') === true) {
		waDialog("force selesai",msg);
    } else
	{
		
	}
});

client.initialize();


const express = require('express')
const app = express()
const port = 8887
const qr = require("qrcode");

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.get('/test/', (req, res) => {
  res.send('Hello TEST !')
})

app.get('/qr/', (req, res) => {
	if (statusWA === "Aktif") {
		res.send(`
		<p>Status : ${statusWA}</p>
		<p><hr/></p>
		<h3>Perintah</h3>
		<ul>
			<li>token : Meminta Token Terbaru</li>
			<li>reset login idsiswa : Reset Login Siswa dengan username atau nama yg mengandung idsiswa</li>
			<li>reset selesai idsiswa : Reset Selesai Siswa dengan username atau nama yg mengandung idsiswa</li>
			<li>force selesai idsiswa : Force Selesai Siswa dengan username atau nama yg mengandung idsiswa</li>
		</ul>
		`)
	} else
	if (qrWA === "") {
		res.send(`Loading ...`);
	} else
	qr.toDataURL(qrWA, (err,src) => {
        if (err) {
			res.send("Error occured");
		} else {
			res.send(`
			<p>Status : ${statusWA}</p>
			<img src=${src} />
			`);
		}
	})
}) 

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})