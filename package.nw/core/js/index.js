function csv2json(s){
    var a = s.split('|');
    var res = [];
    a.forEach(function(row) {
        // check if ; exists
        if (row.indexOf(';') >= 0) {
            let data = [];
            row = row.split(';');
            row.forEach(function(col) {
                data.push(col)
            });
            res.push(data);
        }
    });
    return res;
}

$(document).ready(function () {
    let url = `${localStorage.getItem('urlserver')}wp-content/themes/unbk/api-18575621/showsiswa.php`;
    fetch(url,{
        method: "POST",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        body : `server=${localStorage.getItem("idserver")}`
    })
    .then(res => res.text())
    .then(
        (result) => {
            let data = csv2json(result);
            localStorage.setItem("daftarsiswa", JSON.stringify(data));
        }
    )
});